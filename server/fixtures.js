const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Category = require('./models/Category');
const Product = require('./models/Product');


const run = async () => {
    await mongoose.connect(config.db.url);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [category1, category2, category3, category4] = await Category.create({
        name: "All",
    }, {
        name: "Computers",
    }, {
        name: "Cars",
    }, {
        name: "Other"
    });


    const [user1, user2] = await User.create({
        username: 'admin',
        password: 'admin',
        token: nanoid(),
        displayName: "Azatbek",
        phone: "8-800-555-35-35",
    }, {
        username: 'root',
        password: 'root',
        token: nanoid(),
        displayName: "Kayto",
        phone: "8-800-999-42-42",
    });

    await Product.create({
        title: "HP Victus 16",
        description: "Cool notebook for gaming and work",
        category: category2,
        user: user1,
        price: 92000,
        image: "fixtures/victus_16.jpg",
    }, {
            title: "Boxing gloves",
            description: "Beat somebody's ass",
            category: category1,
            user: user2,
            price: 1500,
            image: "fixtures/boxing.jpg",
        }, {
            title: "Supra A90",
            description: "Fast as fuck",
            category: category3,
            user: user1,
            price: 3000000,
            image: "fixtures/supra.jpg",
        },
        {
            title: "Hat",
            description: "From Harry Potter",
            category: category4,
            user: user2,
            price: 3200,
            image: "fixtures/hat.jpg",
        },
    );



    await mongoose.connection.close();
};

run().catch(console.error);