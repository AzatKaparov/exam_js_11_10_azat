const mongoose = require('mongoose');
const Shema = mongoose.Schema;

const CategoryShema = new Shema({
    name: {
        type: String,
        required: true,
    },
});

const Category = mongoose.model('Category', CategoryShema);
module.exports = Category;