const express = require('express');
const Product = require("../models/Product");
const multer = require('multer');
const config = require('../config');
const {nanoid} = require('nanoid');
const path = require('path');
const auth = require('../middleware/auth');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        let result;
        if (req.query.category) {
            const products = await Product.find({category: req.query.category});
            result = [...products];
        } else {
            result = await Product.find();
        }
        if (!result || result.length === 0) {
            return res.status(404).send({message: "Not found"});
        }
        res.status(200).send(result);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const result = await Product.findOne({_id: req.params.id}).populate([{
            path: "user",
            model: "User",
        }, {
            path: "category",
            model: "Category",
        }]);

        if (!result || result.length === 0) {
            return res.status(404).send({message: "Not found"});
        }
        res.status(200).send(result);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', [auth, upload.single('image')], async (req, res) => {
    try {
        const productData = {
            title: req.body.title,
            description: req.body.description,
            category: req.body.category,
            image: `uploads/${req.file.filename}`,
            user: req.user.id,
            price: req.body.price
        };
        const product = new Product(productData);
        await product.save();
        return res.status(200).send(product);
    } catch (e) {
        console.log(e);
        return res.status(400).send(e);
    }
});


router.delete("/:id", auth, async (req, res) => {
    try {
        const item = await Product.findOne({_id: req.params.id});
        
        if (item.user.equals(req.user._id)) {
            item.delete();
        }

        return res.send(item);
    } catch (e) {
        console.log(e);
    }
});

module.exports = router;