const express = require('express');
const Category = require("../models/Category");

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const result = await Category.find().sort({name: 1});
        if (!result || result.length === 0) {
            return res.status(404).send({message: "Not found"});
        }
        res.status(200).send(result);
    } catch (e) {
        res.sendStatus(500);
    }
});

module.exports = router;