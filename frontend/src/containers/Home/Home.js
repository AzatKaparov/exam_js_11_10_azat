import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Sidebar from "../../components/UI/Sidebar/Sidebar";
import {fetchProducts} from "../../store/actions/productsActions";
import ProductPreview from "../../components/ProductPreview/ProductPreview";
import Backdrop from "../../components/UI/Backdrop/Backdrop";
import Preloader from "../../components/UI/Preloader/Preloader";

const Home = () => {
    const dispatch = useDispatch();
    const { products, loading } = useSelector(state => state.products);

    useEffect(() => {
        dispatch(fetchProducts());
    }, [dispatch]);

    return (
        <div className="container d-flex">
            <Preloader show={loading} />
            <Backdrop show={loading} />
            <Sidebar />
            <div className="content d-flex w-100 flex-wrap justify-content-between">
                    {products.map(product => (
                        <div className="col-6" key={product._id}>
                            <ProductPreview
                                price={product.price}
                                title={product.title}
                                id={product._id}
                                image={product.image}
                            />
                        </div>
                    ))}
                {
                    products.length === 0 && <h1>There is no products in that category</h1>
                }
            </div>
        </div>
    );
};

export default Home;