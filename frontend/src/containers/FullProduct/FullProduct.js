import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {deleteProduct, fetchProduct} from "../../store/actions/productsActions";
import Backdrop from "../../components/UI/Backdrop/Backdrop";
import Preloader from "../../components/UI/Preloader/Preloader";
import {API_URL} from "../../constants";
import {Button} from "react-bootstrap";

const FullProduct = ({match}) => {
    const dispatch = useDispatch();
    const { user } = useSelector(state => state.users);
    const { product, loading } = useSelector(state => state.products);

    const handleDeleteProduct = () => {
        dispatch(deleteProduct(product._id));
    };

    let deleteBtn = <Button onClick={handleDeleteProduct} className="my-3" variant="danger">Удалить</Button>;

    useEffect(() => {
        dispatch(fetchProduct(match.params.id));
    }, [dispatch, user]);

    return product && (
        <>
            <Preloader show={loading} />
            <Backdrop show={loading} />
            <div className="container">
                <div className="d-flex justify-content-center overflow-hidden">
                    <img style={{maxWidth: 500, maxHeight: 500}} src={`${API_URL}/${product.image}`} alt=""/>
                </div>
                <div className="info-block">
                    {user.username === product.user.username
                        ? deleteBtn
                        : null
                    }
                    <h1>{product.title} <span className="text-success small">{product.price}$</span></h1>
                    <h3 className="text-secondary">{product.category.name}</h3>
                    <p>{product.description}</p>
                    <ul>
                        <li>{product.user.displayName}</li>
                        <li>{product.user.phone}</li>
                    </ul>
                </div>
            </div>
        </>
    );
};

export default FullProduct;