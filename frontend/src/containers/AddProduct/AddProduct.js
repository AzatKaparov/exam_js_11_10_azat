import React, {useState} from 'react';
import {Button, Form} from 'react-bootstrap';
import FormElement from "../../components/UI/FormElement/FormElement";
import {useDispatch, useSelector} from "react-redux";
import {createProduct} from "../../store/actions/productsActions";
import Backdrop from "../../components/UI/Backdrop/Backdrop";
import Preloader from "../../components/UI/Preloader/Preloader";

const AddProduct = () => {
    const dispatch = useDispatch();
    const { error, loading } = useSelector(state => state.products);
    const [product, setProduct] = useState({
        title: "",
        description: "",
        image: "",
        category: "",
        price: "",
    });

    const inputChangeHandler = e => {
        const {name, value} = e.target;
        setProduct(prevState => ({...prevState, [name]: value}));
    };

    const onFileChange = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setProduct(prevState => ({
            ...prevState,
            [name]: file
        }));
    };

    const onFormSubmit = async productData => {
        await dispatch(createProduct(productData));
    };

    const submitFormHandler  = e => {
        e.preventDefault();
        const formData = new FormData();

        Object.keys(product).forEach(key => {
            formData.append(key, product[key]);
        });
        onFormSubmit(formData).catch(err => console.error(err));
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <>
            <Preloader show={loading} />
            <Backdrop show={loading} />
            <div className="AddProduct container">
                <Form onSubmit={submitFormHandler}>
                    <FormElement
                        value={product.title}
                        label="Title"
                        required={true}
                        name="title"
                        onChangeHandler={inputChangeHandler}
                        type="text"
                        error={getFieldError("title")}
                    />
                    <FormElement
                        value={product.description}
                        label="Description"
                        as="textarea"
                        required={true}
                        name="description"
                        onChangeHandler={inputChangeHandler}
                        type="text"
                        error={getFieldError("description")}
                    />
                    <Form.Group>
                        <Form.Label>Image</Form.Label>
                        <Form.Control type="file" name="image" required={true} onChange={onFileChange} />
                        {error ? <p>{getFieldError("image")}</p> : null}
                    </Form.Group>
                    <FormElement
                        value={product.category}
                        label="Category"
                        required={true}
                        name="category"
                        onChangeHandler={inputChangeHandler}
                        type="select"
                        select={true}
                        error={getFieldError("category")}
                    />
                    <FormElement
                        label="Price"
                        required={false}
                        name="price"
                        value={product.price}
                        onChangeHandler={inputChangeHandler}
                        type="number"
                        error={getFieldError("price")}
                    />
                    <Button className="my-3" type="submit" variant="primary">Submit</Button>
                </Form>
            </div>
        </>
    );
};

export default AddProduct;