import React, {useState} from 'react';
import {registerUser} from "../../store/actions/usersActions";
import {useDispatch, useSelector} from "react-redux";
import FormElement from "../../components/UI/FormElement/FormElement";
import {Button, Form} from "react-bootstrap";

const Register = () => {
    const dispatch = useDispatch();
    const registerError = useSelector(state => state.users.registerError);
    const [user, setUser] = useState({
        username: "",
        password: "",
        displayName: "",
        phone: "",
    });

    const inputChangeHandler = e => {
        const {name, value} = e.target;
        setUser(prevState => ({...prevState, [name]: value}));
    };

    const submitFormHandler = e => {
        e.preventDefault();
        dispatch(registerUser({...user}));
    };


    const getFieldError = fieldName => {
        try {
            return registerError.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <div className="container">
            <Form onSubmit={submitFormHandler}>
                <FormElement
                    onChangeHandler={inputChangeHandler}
                    name="username"
                    type="text"
                    value={user.username}
                    placeholder="Username"
                    required={true}
                    label="Username"
                    error={getFieldError("username")}
                />
                <FormElement
                    onChangeHandler={inputChangeHandler}
                    name="password"
                    value={user.password}
                    type="password"
                    placeholder="Password"
                    required={true}
                    label="Password"
                    isInvalid={getFieldError("password")}
                />
                <FormElement
                    label="Display name"
                    name="displayName"
                    onChangeHandler={inputChangeHandler}
                    type="text"
                    required={true}
                    placeholder="Display name"
                    value={user.displayName}
                    isInvalid={getFieldError("displayName")}
                />
                <FormElement
                    label="Phone number"
                    name="phone"
                    onChangeHandler={inputChangeHandler}
                    type="tel"
                    required={true}
                    placeholder="Phone"
                    value={user.phone}
                    isInvalid={getFieldError("phone")}
                />
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        </div>
    );
};

export default Register;