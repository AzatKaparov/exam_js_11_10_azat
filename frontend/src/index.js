import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {Provider} from "react-redux";
import {Router} from "react-router-dom";
import store from "./store/configureStore";
import history from "./history";
import 'bootstrap/dist/css/bootstrap.min.css';

const app = (
    <Provider store={store}>
      <Router history={history}>
        <App/>
      </Router>
    </Provider>
);

ReactDOM.render(
    app,
    document.getElementById('root')
);

reportWebVitals();
