import {axiosApi} from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_PRODUCTS_REQUEST = "FETCH_PRODUCTS_REQUEST";
export const FETCH_PRODUCTS_SUCCESS = "FETCH_PRODUCTS_SUCCESS";
export const FETCH_PRODUCTS_FAILURE = "FETCH_PRODUCTS_FAILURE";

export const fetchProductsRequest = () => ({type: FETCH_PRODUCTS_REQUEST});
export const fetchProductsSuccess = products => ({type: FETCH_PRODUCTS_SUCCESS, payload: products});
export const fetchProductsFailure = err => ({type: FETCH_PRODUCTS_FAILURE, payload: err});

export const fetchProducts = (url) => {
    return async dispatch => {
        dispatch(fetchProductsRequest());

        try {
            const response = await axiosApi.get(url ? url : "/products");
            dispatch(fetchProductsSuccess(response.data));
        } catch (e) {
            dispatch(fetchProductsFailure(e));
        }
    };
};

export const CREATE_PRODUCT_REQUEST = "CREATE_PRODUCT_REQUEST";
export const CREATE_PRODUCT_SUCCESS = "CREATE_PRODUCT_SUCCESS";
export const CREATE_PRODUCT_FAILURE = "CREATE_PRODUCT_FAILURE";

export const createProductRequest = () => ({type: CREATE_PRODUCT_REQUEST});
export const createProductSuccess = product => ({type: CREATE_PRODUCT_SUCCESS, payload: product});
export const createProductFailure = err => ({type: CREATE_PRODUCT_FAILURE, payload: err});

export const createProduct = (productData) => {
    return async (dispatch, getState) => {
        dispatch(createProductRequest());

        try {
            const response = await axiosApi.post(
                "/products",
                productData,
                {headers: {
                    "Authorization": getState().users.user.token,
                    }}
                );
            dispatch(createProductSuccess(response.data));
            dispatch(historyPush(`/product/${response.data._id}`));
        } catch (e) {
            dispatch(createProductFailure(e));
        }
    };
};

export const FETCH_PRODUCT_REQUEST = "FETCH_PRODUCT_REQUEST";
export const FETCH_PRODUCT_SUCCESS = "FETCH_PRODUCT_SUCCESS";
export const FETCH_PRODUCT_FAILURE = "FETCH_PRODUCT_FAILURE";

export const fetchProductRequest = () => ({type: FETCH_PRODUCT_REQUEST});
export const fetchProductSuccess = product => ({type: FETCH_PRODUCT_SUCCESS, payload: product});
export const fetchProductFailure = err => ({type: FETCH_PRODUCT_FAILURE, payload: err});

export const fetchProduct = (id) => {
    return async dispatch => {
        dispatch(fetchProductRequest());

        try {
            const response = await axiosApi.get(`/products/${id}`);
            dispatch(fetchProductSuccess(response.data));
        } catch (e) {
            dispatch(fetchProductFailure(e));
        }
    };
};


export const DELETE_PRODUCT_REQUEST = "DELETE_PRODUCT_REQUEST";
export const DELETE_PRODUCT_SUCCESS = "DELETE_PRODUCT_SUCCESS";
export const DELETE_PRODUCT_FAILURE = "DELETE_PRODUCT_FAILURE";

export const deleteProductRequest = () => ({type: DELETE_PRODUCT_REQUEST});
export const deleteProductSuccess = product => ({type: DELETE_PRODUCT_SUCCESS, payload: product});
export const deleteProductFailure = err => ({type: DELETE_PRODUCT_FAILURE, payload: err});

export const deleteProduct = (id) => {
    return async (dispatch, getState) => {
        dispatch(deleteProductRequest());

        try {
            const response = await axiosApi.delete(
                `/products/${id}`,
                {headers: {
                    "Authorization": getState().users.user.token
                    }}
            );
            dispatch(deleteProductSuccess(response.data));
            dispatch(historyPush("/"));
        } catch (error) {
            if (error.response && error.response.data) {
                console.log(error.response.data);
                dispatch(deleteProductFailure(error.response.data));
            } else {
                dispatch(deleteProductFailure({message: "No internet!"}))
            }
        }
    };
};