import {
    FETCH_CATEGORIES_FAILURE,
    FETCH_CATEGORIES_REQUEST,
    FETCH_CATEGORIES_SUCCESS
} from "../actions/categoriesActions";

const initialState = {
    error: null,
    loading: null,
    categories: [],
};

const categoriesReducer = (state=initialState, action) => {
    switch (action.type) {
        case FETCH_CATEGORIES_REQUEST:
            return {...state, loading: true};
        case FETCH_CATEGORIES_SUCCESS:
            return {...state, loading: true, categories: action.payload};
        case FETCH_CATEGORIES_FAILURE:
            return {...state, loading: false, error: action.payload};
        default:
            return state;
    }
};

export default categoriesReducer;