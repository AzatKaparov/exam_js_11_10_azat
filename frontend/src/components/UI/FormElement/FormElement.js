import React, {useEffect} from 'react';
import {Form} from "react-bootstrap";
import PropTypes from 'prop-types';
import {useDispatch, useSelector} from "react-redux";
import {fetchCategories} from "../../../store/actions/categoriesActions";

const FormElement = ({ onChangeHandler, name, value, type, placeholder, required, label, as, select, error }) => {
    const dispatch = useDispatch();
    const { categories } = useSelector(state => state.categories);

    useEffect(() => {
        dispatch(fetchCategories());
    }, [dispatch]);


    if (select) {
        return (
            <>
                <Form.Label className="d-block">{label}</Form.Label>
                <Form.Select name={name} className="p-2 d-block" onChange={onChangeHandler}>
                    <option>Please select a category</option>
                    {categories.map(item => (
                        <option key={item._id} value={item._id}>{item.name}</option>
                    ))}
                </Form.Select>
            </>
        )
    }


    return (
        <Form.Group className="mb-3">
            <Form.Label>{label}</Form.Label>
            <Form.Control
                onChange={onChangeHandler}
                name={name}
                value={value ? value : ""}
                required={required}
                type={type}
                placeholder={placeholder}
                as={as}
            />
            { error ? <p>{error}</p> : null}
        </Form.Group>
    );
};

FormElement.propTypes = {
    onChangeHandler: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    required: PropTypes.bool.isRequired,
    label: PropTypes.string.isRequired,
    as: PropTypes.string
};

export default FormElement;