import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import SidebarItem from "../SidebarItem/SidebarItem";
import {fetchCategories} from "../../../store/actions/categoriesActions";
import './Sidebar.css';

const Sidebar = () => {
    const dispatch = useDispatch();
    const { categories } = useSelector(state => state.categories);

    useEffect(() => {
        dispatch(fetchCategories());
    }, [dispatch]);


    return (
        <div className="Sidebar border">
            <ul>
                {categories.map(item =>  (
                    <SidebarItem
                        id={item._id}
                        to={`/${item.name.toLowerCase()}`}
                        key={item._id}
                        title={item.name}
                    />
                ))}
            </ul>
        </div>
    );
};

export default Sidebar;