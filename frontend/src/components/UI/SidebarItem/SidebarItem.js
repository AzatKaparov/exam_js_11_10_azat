import React from 'react';
import {NavLink} from "react-router-dom";
import {useDispatch} from "react-redux";
import {fetchProducts} from "../../../store/actions/productsActions";

const SidebarItem = ({title, to, id}) => {
    const dispatch = useDispatch();

    const handleCategoryLink = e => {
        e.preventDefault();
        let url;
        if (title.toLowerCase() === "all") {
            url = '/products';
        } else {
            url = `/products?category=${id}`
        }
        dispatch(fetchProducts(url));
    };

    return (
        <li>
            <NavLink onClick={handleCategoryLink} to={to}>{title}</NavLink>
        </li>
    );
};

export default SidebarItem;