import React from 'react';
import {Card} from "react-bootstrap";
import PropTypes from "prop-types";
import {API_URL} from "../../constants";
import {NavLink} from "react-router-dom";

const ProductPreview = ({image, title, price, id}) => {
    return (
        <Card className="mb-3 d-flex flex-column align-item-start">
            <Card.Img className="align-self-center" style={{maxWidth: 300, maxHeight: 300}} variant="top" src={`${API_URL}/${image}`} />
            <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Card.Text>
                    Price: {price}
                </Card.Text>
                <NavLink to={`/product/${id}`} variant="primary" className="flex-grow-1 btn btn-primary">More</NavLink>
            </Card.Body>
        </Card>
    );
};

ProductPreview.propTypes = {
    title: PropTypes.string.isRequired,
    image: PropTypes.string,
    price: PropTypes.number.isRequired
};

export default ProductPreview;