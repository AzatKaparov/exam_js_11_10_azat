import {Route, Switch} from "react-router-dom";
import Home from "./containers/Home/Home";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./components/UI/Layout/Layout";
import AddProduct from "./containers/AddProduct/AddProduct";
import FullProduct from "./containers/FullProduct/FullProduct";

function App() {
  return (
    <div className="App">
      <Layout>
        <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/add-product" exact component={AddProduct}/>
          <Route path="/product/:id" exact component={FullProduct}/>
          <Route path="/register" exact component={Register}/>
          <Route path="/login" exact component={Login}/>
          <Route render={() => <h1>Not found</h1>}/>
        </Switch>
      </Layout>
    </div>
  );
}

export default App;
